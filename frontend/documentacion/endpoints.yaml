openapi: 3.0.0
info:
  description: |
    Aplicación de streaming de música, gratuito. Proyecto final de Seminario, parte
    del movimiento Cloud Innovation. Actualmente se encuentra desplegado una versión de prueba, sin validaciones e
    implementada de tal forma que funcione únicamente de manera básica. Los datos son obtenidos directamente de la
    base de datos, no son quemados.
  version: "1.0.0"
  title: R-7
  contact:
    email: renatojosuefloresperez@gmail.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
servers:
  - url: 'http://132.145.164.171:1989'
tags:
  - name: R-7
    description: Stream music for free. For the community, by the community.
    externalDocs:
      description: Puedes revisar la documentación oficial del portal en el repositorio oficial de Gitlab.
      url: https://gitlab.com/renato776/r-7/-/tree/master/frontend

paths:
  /login:
    post:
      tags:
        - users
      summary: Logea un usuario al sistema.
      operationId: login
      responses:
        '200':
          description: El usuario existe y sus credenciales son correctas.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        '400':
          description: Incorrect credentials.
      requestBody:
        $ref: '#/components/requestBodies/Login'
  /register:
    post:
      tags:
        - users
      summary: Registra un nuevo usuario al sistema.
      operationId: register
      requestBody:
        $ref: '#/components/requestBodies/Register'
      responses:
        '201':
          description: OK
        '400':
          description: Username already exists.
        '500':
          description: Error on server.
  /feed:
    post:
      tags:
        - songs
        - home
      summary: Obtiene la lista de canciones a presentar al usuario en su home.
      description: |
        Obtiene el feed del grid a presentar al usuario cuando entra a su home. Implementa paginación del lado del servidor. En teoría,
        debería obtener el feed personalizado para el usuario que inicia sesión. Actualmente, se hace una query simple a la tabla feed.
        Una tabla temporal que realiza todos los joins necesarios, usa como llave primaria el src de una canción y se ordena en base a la
        columna orden. En teoría debe dejar de ser consultada y obtener el feed de Redis. El feed debe poder implementarse en base a los
        taps de cada usuario. La definición de tap se explica en su endpoint asociado.
      operationId: feed
      requestBody:
        $ref: '#/components/requestBodies/Feed'
      responses:
        '200':
          description: |
            Este endpoint no debería fallar nunca. 
            Si un usuario recien ha iniciado sesión 
            por primera vez y no se tiene información 
            de sus taps, entonces se deberán mostrar
            las canciones más populares de la
            plataforma.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Feed'
  /tap/{uid}/{sid}:
    get:
      tags:
        - songs
        - recommendations
      summary: Registra el interés de un usuario por una determinada canción.
      description: |
        Registra una operación tap para el usuario y canción asociadas. Un tap es simplemente un click el usuario hace a alguna canción e 
        indica al sistema que el usuario tiene interés por dicha canción. Los taps deben ser registrados por el sistema para armar el feed
        asociado. Un tap incrementa en 1 el tap count de un usuario y debe mantenerse en el backend entre sesiones.
      operationId: tap
      parameters:
        - name: uid
          in: path
          description: ID del usuario que realizó la operacion.
          required: true
          schema:
            type: integer
            format: int64
        - name: sid
          in: path
          description: ID de la canción asociada.
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: Este endpoint no debería fallar nunca, media vez el uid y sid sean válidos.
        '400':
          description: Uid o Sid invalidos.
  /search:
    post:
      tags:
        - songs
        - home
      summary: Busca música por artista, album o nombre de canción.
      description: |
        Busca música en la biblioteca de la plataforma. La búsqueda se implementa con el operador de Postgres ILIKE para realizar una 
        búsqueda case insensitive en cualquier parte de la string (i.e artist ilike '%hal%') utilizar los operadores % para realizar la
        búsqueda por substrings y no solo por match completo. La búsqueda debe ser tanto por nombre de canción, como por nombre de album
        o nombre de artista.
      responses:
        '200':
          description: Retorna el feed asociado a la operación. Un array vacío si no hay ningún match.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Feed'
      requestBody:
        $ref: '#/components/requestBodies/Search'
  /translate:
    post:
      tags:
        - translate
      summary: Traduce la letra de una canción
      description: |
       Traduce la letra de una canción. El texto a traducir es libre y se envía como parte del body de la request, no es necesario buscar
        la letra asociada de la canción. El idioma origen también es libre y se indica en el body. El idioma target también es variable.
      responses:
        '200':
          description: Traducción exitosa.
          content:
            application/json:
              schema:
                type: object
                properties:
                  code: 
                    type: integer
                  data:
                    type: object
                    properties:
                      translate:
                        type: string
        '400':
          description: Idioma origen o idioma destino no soportados (imposible que el frontend haga una petición de esa manera).
      requestBody:
        $ref: '#/components/requestBodies/Translate'
  /chatbot:
    post:
      tags:
        - chatbot
      summary: Envía un mensaje al chatbot del sistema.
      description: |
        Envía un mensaje al chatbot de la aplicación. Difiere al chatbot de Ugram en el sentido que este chatbot solo da información general
        de la aplicación, como canciones más populares, últimas canciones en ser agregadas, etc. Las respuestas pueden ser random, cuidando
        que tengan sentido relativo, ya que no hay forma de saber si las canciones indicadas por el chatbot son realmente las más populares
        o no. Claro, en teoría lo mejor sería que se implemente de forma apropiada. Finalmente, es importante notar que no pide ID del usuario
        realizando la petición, ya que el chatbot es general y no le interesa saber quien hace la petición.
      responses:
        '200':
          description: Mensaje respondido exitosamente.
          content:
            application/json:
              schema:
                type: object
                properties:
                  code: 
                    type: integer
                  data:
                    type: object
                    properties:
                      message: 
                        type: string
        '500':
          description: Error en el servidor (imposible de obtener, en teoría).
      requestBody:
        $ref: '#/components/requestBodies/Chatbot'



components:
  schemas:
    User:
      type: object
      properties:
        id: 
          type: integer
          format: int32
    Song:
      type: object
      properties:
        img:
          type: string
          format: url
        artist:
          type: string
        title:
          type: string
        lyrics:
          type: string
        lang:
          type: string
        album_name:
          type: string
    Feed:
      type: array
      items:
        $ref: '#/components/schemas/Song'
  requestBodies:
    Login:
      content:
        application/json:
          schema:
            type: object
            properties:
              username:
                type: string
              password:
                type: string
      description: |
        La password se envía como
        plain-text, el servidor es el encargado
        de realizar el encriptado a md5 para la comparación con la DB.
      required: true
    Register:
      content:
        application/json:
          schema:
            type: object
            properties:
              name:
                type: string
              username:
                type: string
              password:
                type: string
              confirmation_password:
                type: string
      description: |
        Password y confirmation_password deben coincidir. Se encriptan en el servidor, por lo que se envían como texto plano.
        El username debe ser único.
      required: true
    Feed:
      content:
        application/json:
          schema:
            type: object
            properties:
              uid:
                type: integer
              size:
                type: integer
              offset:
                type: integer
      description: |
        Size se refiere al número de records a recibir de la DB, se traduce a limit en Postgres. Offset significa igual que en postgres.
        El feed, en teoría, debería ser consultado de Redis, por lo que el significado de estos parámetros es el mismo, pero su
        implementación deberá adecuarse.
      required: true
    Tap:
      content:
        application/json:
          schema:
            type: object
            properties:
              uid:
                type: integer
              song:
                type: integer
      description: Uid y song se refieren a los IDs de cada uno.
      required: true
    Search:
      content:
        application/json:
          schema:
            type: object
            properties:
              txt:
                type: string
      description: Término a buscar por el usuario.
      required: true
    Translate:
      content:
        application/json:
          schema:
            type: object
            properties:
              source:
                type: string
              txt:
                type: string
              language:
                type: integer
                format: int64
      description: |
        El source se refiere al código de idioma que espera AWS que identifica el lenguaje en el que se encuentra la letra de la 
        canción actualmente y se encuentra ya adecuadamente definido (en para ingles, ko para coreano, etc). No
        es necesario mapearlo de un código numérico a su código de Translate. Txt es la string a traducir y finalmente language si 
        es de tipo integer ya que utiliza la implementación vieja y se mapea internamente a su código de idioma. Los códigos numéricos
        son los mismos, ruso mantiene 3 y se agregó el 4 para Coreano. Indica el target language al que se desea traducir.
      required: true
    Chatbot:
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: string
      description: Mensaje a envíar al chatbot.
      required: true
