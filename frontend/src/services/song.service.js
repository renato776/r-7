import axios from 'axios';

let url = process.env.MOCK_SERVER_URL ||'http://150.136.49.70:1989';
let og_url = process.env.VUE_APP_HTTP;

export default {

    getRecommended( uid, size, offset) {
        return axios.post( url + '/feed/', { uid: uid, size: size, offset: offset });
    },

    tap(uid, song){
        return axios.post( url + '/tap/', { uid: uid, song: song } );
    },

    lyrics(song){
        return axios.post( url + '/lyrics/', { song: song } );
    },

    search( txt ){
        return axios.post( url + '/search/', { txt: txt } );
    },

    translate( txt, language, og_lang){
        return axios.post( og_url + '/lyrics/translate/', { source: og_lang,
          txt: typeof txt === "string"? txt : txt.map(line => line.stmt).join('\n'), language: language} );
    }
}
