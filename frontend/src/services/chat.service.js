import axios from 'axios';

let url = process.env.VUE_APP_HTTP;

export default {

    chat(message, id) {
        return axios.put(url + '/chatbot/' + id, { message: message });
    }

}
