import axios from 'axios';

let url = process.env.VUE_APP_HTTP;

export default {

    login(username, password) {
        return axios.post(url + '/login', {
            username: username,
            password: password
        });
    },

    register(name, username, password, passwordConfirmation) {
        return axios.post(url + '/users', {
            name: name,
            username: username,
            password: password,
            confirmation_password: passwordConfirmation
        });
    }
}
