import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function next_song(src, songs){
  for(let i=0; i<songs.length; i++){
    const song = songs[i];
    if(song.src == src){
      if(i+1<songs.length)return songs[i+1];
      else return null;
    }
  }
  return null;
}
function toMinutes(seconds){
  const mins = Math.floor(seconds/60);
  const secs = (seconds - mins*60).toFixed();
  return `${mins}:${secs}`;
}

export default new Vuex.Store({
  state: {
    user: localStorage.getItem('session') || null,
    track: null,
    anchor: null,
    songs: [],
    song: {
      img: null,
      artist: '',
      title: '',
      lyrics: '',
      lang: '',
      album_name: ''
    }
  },

  getters: {

    getSession(state) {
      if (state.user) return JSON.parse(localStorage.getItem('session'));
      return null;
    },

    song_name(state){ return state.song.title; },
    song_lang(state){ return state.song.lang; },
    song_artist(state){ return state.song.artist; },
    song_img(state){ return state.song.img; },
    song_lyrics(state){ 
      if (!state.song.lyrics) return [{id: 1, stmt: 'Not available'}];
      if (state.song.lyrics.length < 1) return [{id: 1, stmt: 'Not available'}];
      let i = 0;
        return state.song.lyrics.split('\n').map(stmt => {
          i++;
          return {id: i, stmt: stmt}
        });
    },
    song_album_name(state){ return state.song.album_name; },
    get_track(state){ return state.track; },
    get_songs(state){ return state.songs; }

  },

  mutations: {
    anchor(state, ren){
        state.anchor = ren;
    },
    translate(state, jk){
        console.log("Updating language to:", jk['nl']);
        state.song.lyrics = jk['l'];
        state.song.lang = jk['nl'];
    },
    set_songs(state,songs){
      state.songs = songs;
    },
    setSong(state, payload){
      if(!payload)return;
      const song = payload.sig;
      if(!song)return;
      if(!state.anchor)return;

      if(!state.song){
        state.song = {};
      }

      state.song.img = song.img;
      state.song.artist= song.artist;
      state.song.title= song.title;
      state.song.lyrics= song.lyrics;
      state.song.lang= song.lang;
      state.song.album_name= song.album_name;

      if(!state.track) state.track = new Audio(song.src);
      else {
        state.track.setAttribute("src", song.src);
        state.track.load();
        try{
          state.track.play();
        } catch(e){
          console.log('Failed to autoplay track');
        }
      }

      state.track.addEventListener('timeupdate',
        function () { 
          state.anchor.timestamp = toMinutes(state.track.currentTime);
          state.anchor.progress = 
            `${((state.track.currentTime/state.track.duration)*100).toFixed()}%`
      });
      state.track.addEventListener('pause', () => {
        state.anchor.playing = false;
      });
      state.track.addEventListener('play', () => {
        state.anchor.playing = true;
      });
      state.track.onended = () => {
        const n = next_song(song.src, state.songs);
        if(n) payload.anchor.commit('setSong', {sig: n, anchor: payload.anchor});
      };

    },

    login(state, user) {
      localStorage.setItem('session', JSON.stringify(user));
      state.user = user;
    },

    logout(state) {
      localStorage.removeItem('session');
      state.user = null;
    }
  }

})
